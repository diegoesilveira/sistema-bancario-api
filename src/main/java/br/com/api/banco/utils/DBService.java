package br.com.api.banco.utils;

import java.text.ParseException;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.banco.domain.Cliente;
import br.com.api.banco.domain.enums.TipoCliente;
import br.com.api.banco.repositories.ClienteRepository;

@Service
public class DBService {

	
	@Autowired
	private ClienteRepository clienteRepository;

	
	
	public void instantiateTestDatabase() throws ParseException {
		
		
		
		Cliente cli1 = new Cliente(null, "Diego Silveira", TipoCliente.PESSOAFISICA, "36378912377");
		clienteRepository.saveAll(Arrays.asList(cli1));
		
	}
}
