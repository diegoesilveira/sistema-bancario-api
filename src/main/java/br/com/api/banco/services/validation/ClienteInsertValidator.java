package br.com.api.banco.services.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.api.banco.domain.Cliente;
import br.com.api.banco.domain.enums.TipoCliente;
import br.com.api.banco.resources.exception.FieldMessage;
import br.com.api.banco.services.validation.utils.BR;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, Cliente> {

	
	@Override
	public void initialize(ClienteInsert ann) {
	}

	@SuppressWarnings("unlikely-arg-type")
	@Override
	public boolean isValid(Cliente obj, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();

		if (obj.getTipo().equals(TipoCliente.PESSOAFISICA.getCod()) && !BR.isValidCPF(obj.getNumeroDocumento())) {
			list.add(new FieldMessage("Documento", "CPF inválido"));
		}

		if (obj.getTipo().equals(TipoCliente.PESSOAJURIDICA.getCod()) && !BR.isValidCNPJ(obj.getNumeroDocumento())) {
			list.add(new FieldMessage("Documento", "CNPJ inválido"));
		}

		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	
	}
}
