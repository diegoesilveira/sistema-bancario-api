package br.com.api.banco.services;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.api.banco.domain.Cliente;
import br.com.api.banco.repositories.ClienteRepository;
import br.com.api.banco.services.exceptions.DataIntegrityException;
import br.com.api.banco.services.exceptions.ObjectNotFoundException;
import br.com.api.banco.utils.RandomScoreCliente;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository repo;
	
	@Autowired
	private RandomScoreCliente scoreCliente;
	
	public Cliente find(Long id) {
		Optional<Cliente> cliente = repo.findById(id);
		return cliente.orElseThrow(() -> new ObjectNotFoundException("Cliente não encontrado! Id: " + id + ", Tipo: " + Cliente.class.getName())); 
	}

	public Cliente insert(Cliente id) {
		id.setId(null);
		id.setScoreCliente(scoreCliente.getScore());		
		return repo.save(id);
		
	}
	
	public Cliente update(Cliente id) {
		return repo.save(id);
	}
	
	public void delete(Long id) {
		find(id);
		try {
			repo.deleteById(id);
		}catch(DataIntegrityException e) {
			throw new DataIntegrityException("Não foi possível remover, cliente vinculado a uma conta corrente. ");
		}
	}
	
	public List<Cliente> findAll(){
		return repo.findAll();
	}
}
