package br.com.api.banco.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.com.api.banco.domain.enums.TipoCliente;
import br.com.api.banco.services.validation.ClienteInsert;

@Entity
@ClienteInsert
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 4, max = 120, message = "O tamanho deve ser entre 4 e 120 caracteres")
	private String nome;

	private Integer tipo;

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 11, max = 14, message = "Verifique os dados digitados, CPF 11 caracteres e CNPJ 14 caracteres")
	@Column(unique = true)
	private String numeroDocumento;

	private Integer scoreCliente;

	public Cliente() {
	}

	public Cliente(Long id, Integer scoreCliente) {
		this.id = id;
		this.scoreCliente = scoreCliente;
	}

	public Cliente(Long id, String nome, TipoCliente tipo, String numeroDocumento) {
		this.id = id;
		this.nome = nome;
		this.tipo = (tipo == null) ? null : tipo.getCod();
		this.numeroDocumento = numeroDocumento;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Enumerated(EnumType.ORDINAL)
	public TipoCliente getTipo() {
		return TipoCliente.toEnum(tipo);
	}

	public void setTipo(TipoCliente tipo) {
		this.tipo = tipo.getCod();
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public void setScoreCliente(Integer scoreCliente) {
		this.scoreCliente = scoreCliente;
	}

	public Integer getScoreCliente() {
		return scoreCliente;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
